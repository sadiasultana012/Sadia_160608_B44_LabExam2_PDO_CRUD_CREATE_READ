<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";




$objOrganizationSummary = new \App\OrganizationSummary\OrganizationSummarys();
$objOrganizationSummary->setData($_GET);
$oneData = $objOrganizationSummary->view();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>OrganizationSummary Edit Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

    <div class="navbar">

        <td>
            <a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a>
            <a href='create.php' class='btn btn-group-lg btn-info'>Reload</a>

        </td>

    </div>



    <form  class="form-group f" action="update.php" method="post">

        Please Enter Organization Name:
        <input type="text" name="orgName"  value="<?php echo $oneData->orgName ?>">
        <br>
        Please Enter Type:
        <input type="text" name="orgType" value="<?php echo $oneData->type ?>">
        <br>
        Please Enter Owner Name:
        <input type="text" name="ownerName" value="<?php echo $oneData->owner_name ?>">
        <br>
        Please Enter Foundation Time:
        <input type="date" name="foundationTime" value="<?php echo $oneData->founding_time ?>">
        <br>
        Please Enter Mission:
        <input type="text" name="orgMission" value="<?php echo $oneData->mission ?>">
        <br>

        <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >

        <input class="btn btn-primary" type="submit" value="Update">

    </form>

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


