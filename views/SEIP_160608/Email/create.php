<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div id='message'> $msg </div>";

?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>Email Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <script
        src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="
        crossorigin="anonymous"></script>


</head>
<body>




    <form action = "store.php" method = "post">
        Please Enter Person's Name:
        <br>
        <input type = "text" name="name" method = "post">
        <br><br>
        Email:<br> <input type="text" name="email"  method="post">
        <br><br>
        <input type="submit">
        <br>

    </form>

    <div class="navbar">
        <td><a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a> </td><br><br>
        <td><a href='create.php' class='btn btn-group-lg btn-info'>Reload</a> </td>

    </div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>


</body>
</html>