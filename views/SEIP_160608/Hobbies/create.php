<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::message();

echo "<div id='message'> $msg </div>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>


<form action = "store.php" method = "post">
    Please Enter Student's Name:
    <br>
    <input type = "text" name="studentName" method = "post">
    <br>
    Hobbies: <input type="checkbox" name="hobby[]" value="Travelling" method="post"><label>Travelling</label>
    <input type="checkbox" name="hobby[]" value="Reading"  method="post"><label>Reading</label>
    <input type="checkbox" name="hobby[]" value="Photography"  method="post"><label>Photography</label><br>
    <input type="submit">
    <br>

</form>

<div class="navbar">
    <td><a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a> </td><br><br>
    <td><a href='create.php' class='btn btn-group-lg btn-info'>Reload</a> </td>

</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>


</body>
</html>