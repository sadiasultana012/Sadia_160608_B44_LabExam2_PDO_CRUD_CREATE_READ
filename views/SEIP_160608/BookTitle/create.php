<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Add Form</title>
</head>
<body>

<form action="store.php" method="post">
    Please Enter Book Name:<br>
    <input type="text" name="bookName">
    <br>
    Please Enter Author Name:<br>
    <input type="text" name="authorName">
    <br><br>

    <input type="submit">

</form>

<div class="navbar">
    <td><a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a> </td><br>

    <td><a href='create.php' class='btn btn-group-lg btn-info'>Reload</a> </td>

</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>