<?php

require_once("../../../vendor/autoload.php");

$objGender = new \App\Gender\Gender();

$_POST["gender"] = implode(",", $_POST["gender"]);

$objGender->setData($_POST);

$objGender->store();

?>