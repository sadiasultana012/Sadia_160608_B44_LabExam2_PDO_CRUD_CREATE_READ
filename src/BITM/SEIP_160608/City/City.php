<?php
namespace App\City;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO, PDOException;
class City extends DB
{
    private $id;
    private $cityName;

    public function setData($postData)
    {

        if (array_key_exists('id', $postData)) {
            $this->id = $postData['id'];
        }

        if (array_key_exists('city', $postData)) {
            $this->cityName = $postData['city'];
        }

    }


    public function store()
    {

        $arrData = array($this->cityName);

        $sql = "INSERT into city(name) VALUES(?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if ($result)
            Message::message("<div id='msg'>Success! Data Has Been Inserted Successfully :)</div>");
        else
            Message::message("<div id='msg'>Failed! Data Has Not Been Inserted Successfully :( </div>");

        Utility::redirect('create.php');

    }

    public function index(){

        $sql = "select * from city where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from city where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from city where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

    public function update(){

        $arrData = array($this->cityName);

        $sql = "UPDATE  city SET name=? WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('index.php');


    }



    public function trash(){

        $sql = "UPDATE city SET soft_deleted='Yes' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('index.php');


    }


    public function recover(){

        $sql = "UPDATE city SET soft_deleted='No' WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);


        if($result)
            Message::message("Success! Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php');


    }




    public function delete(){

        $sql = "Delete from city  WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");


        Utility::redirect('index.php');


    }


    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from city  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }

    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from city  WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }

}